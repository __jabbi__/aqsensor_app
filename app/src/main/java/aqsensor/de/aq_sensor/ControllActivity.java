package aqsensor.de.aq_sensor;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.todddavies.components.progressbar.ProgressWheel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;

public class ControllActivity extends AppCompatActivity {

    public BluetoothSocket btSocket;
    public OutputStream outputStream;
    public InputStream inputStream;
    public int tempI = 0;
    public int humiI = 0;
    public int gasCI = 0;
    public TextView tempText;
    public TextView humiText;
    public TextView gasCText;
    public ProgressWheel pw;
    public ProgressWheelState pws = ProgressWheelState.bad;
    Handler mHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            final byte[] writeBuf = (byte[]) msg.obj;
            int begin = (int) msg.arg1;
            int end = (int) msg.arg2;

            String writeMessage = new String(writeBuf, StandardCharsets.UTF_8);
            writeMessage = writeMessage.substring(begin, end);
            Log.e("Data recievend", writeMessage);
            if(writeMessage.charAt(0) == 'd') {
                String[] parts = writeMessage.split(";");
                Log.i("Data accept", "writing variables");
                Log.i("parts Lengt", String.valueOf(parts.length));
                if (parts.length == 5) {
                    try {
                        tempI = Integer.parseInt(parts[1]);
                        humiI = Integer.parseInt(parts[2]);
                        gasCI = Integer.parseInt(parts[3]);
                        switch(parts[4]) {
                            case "good":
                                pws = ProgressWheelState.good;
                                break;
                            case "middle":
                                pws = ProgressWheelState.middle;
                                break;
                            case "bad":
                                pws = ProgressWheelState.bad;
                                break;
                        }

                    } catch (NumberFormatException e) {
                        Toast.makeText(getApplicationContext(), "Fehler bei der Antwort", Toast.LENGTH_LONG).show();
                        Log.e("Error", "Recieve Data is Wrong, not int");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Fehler bei der Antwort", Toast.LENGTH_LONG).show();
                }
                updateUi();
                checkData();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controll);
        setTitle("Sensor Control");
        btSocket = data.btSocket;

        pw = (ProgressWheel) findViewById(R.id.mainWheel);

        pw.startSpinning();
        pw.setBarLength(1000);

        humiText = (TextView) findViewById(R.id.humi);
        gasCText = (TextView) findViewById(R.id.gasC);
        tempText = (TextView) findViewById(R.id.temp);

        try {
            inputStream = btSocket.getInputStream();
            outputStream = btSocket.getOutputStream();
        } catch (IOException e) { Log.e("ControllActivity", "IOexeption"); }

        checkData();


    }
    public void checkData()
    {
        if(btSocket.isConnected()) {
            ConnectedThread connection = new ConnectedThread();
            connection.start();
            connection.write("getData".getBytes());
        }else{
            Toast.makeText(getApplicationContext(), "Socket ist tot, ruhe er in Frieden",Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    public void updateUi()
    {
        Log.i("updateUI", "setting new Variables");
        Log.i("variables", tempI + " " + humiI + " " + " " + gasCI);
        switch (pws){
            case good:
                pw.setBarColor(Color.GREEN);
                pw.setText("Gute Luftqualität");
                break;
            case middle:
                pw.setBarColor(Color.YELLOW);
                pw.setText("Bald Fenster aufmachen!");
                break;
            case bad:
                pw.setBarColor(Color.RED);
                pw.setText("Fenster aufmachen!!!");
                break;
        }
        tempText.setText(String.valueOf(tempI));
        humiText.setText(String.valueOf(humiI));
        gasCText.setText(String.valueOf(gasCI));
    }


    private class ConnectedThread extends Thread {

        public
        ConnectedThread() {
            super();
        }
        public void run() {
            Log.e("started Thread", "ControllActivity");
            byte[] buffer = new byte[1024];
            int begin = 0;
            int bytes = 0;
            while (true) {
                try {
                    bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                    for(int i = begin; i < bytes; i++) {
                        if(buffer[i] == "#".getBytes()[0]) {
                            Log.e("Starting Handler", "found");
                            mHandler.obtainMessage(1, begin, i, buffer).sendToTarget();
                            begin = i + 1;
                            if(i == bytes - 1) {
                                bytes = 0;
                                begin = 0;
                            }
                        }
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }
        public void write(byte[] bytes) {
            try {
                Log.e("write", new String(bytes));
                outputStream.write(bytes);
            } catch
                    (IOException e) { }
        }
        public void cancel() {
            try {
                btSocket.close();
            } catch (IOException e) { }
        }
    }


}

